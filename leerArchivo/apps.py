from django.apps import AppConfig


class LeerarchivoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'leerArchivo'
